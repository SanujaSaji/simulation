package com.thoughtworks.paysa.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.thoughtworks.paysa.exception.WalletByNameAlreadyExists;
import com.thoughtworks.paysa.model.User;
import com.thoughtworks.paysa.model.Wallet;
import com.thoughtworks.paysa.repository.UserRepository;
import com.thoughtworks.paysa.repository.WalletRepository;
import com.thoughtworks.paysa.service.WalletService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.Arrays;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class WalletControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    private
    WalletService walletService;

    @Autowired
    private
    UserRepository userRepository;

    @Autowired
    private
    WalletRepository walletRepository;

    @Test
    void shouldGetWalletObjectIfFound() throws Exception {
        User user = userRepository.save(new User("admin", "password", "John", "Doe"));
        Wallet wallet = new Wallet("admin", 100, user);
        walletRepository.save(wallet);

        when(walletService.get(1)).thenReturn(wallet);

        mockMvc.perform(get("/users/1/wallets/1")).
                andExpect(status().isOk())
                .andExpect(jsonPath("$.balance").value("100.0"))
                .andExpect(jsonPath("$.name").value("admin"));
    }

    @Test
    void shouldAddMoneyToWalletAndReturnUpdatedWallet() throws Exception {
        User user = userRepository.save(new User("admin", "password", "John", "Doe"));
        Wallet adminWallet = new Wallet("admin-wallet", 100, user);
        walletRepository.save(adminWallet);
        Wallet updatedWallet = new Wallet("admin-wallet", 400, user);
        ObjectMapper objectMapper = new ObjectMapper();
        when(walletService.addMoney(adminWallet.getId(), 300)).thenReturn(updatedWallet);

        mockMvc.perform(put("/users/1/wallets/1")
                .content(objectMapper.writeValueAsString(updatedWallet))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        verify(walletService, times(1)).addMoney(any(Integer.class), any(Double.class));
    }

    @Test
    void shouldAddWalletIfNameValid() throws Exception {
        User user = userRepository.save(new User("admin", "password", "John", "Doe"));
        Wallet wallet = new Wallet("wallet", 400, user);
        ObjectMapper objectMapper = new ObjectMapper();
        when(walletService.save(wallet)).thenReturn(wallet);

        mockMvc.perform(post("/users/1/wallets")
                .content(objectMapper.writeValueAsString(wallet))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());

        verify(walletService, times(1)).save(any(Wallet.class));
    }

    @Test
    void shouldSendForbiddenResponseIfNameIsNotUnique() throws Exception {
        Wallet wallet = new Wallet("wallet", 400, new User());
        ObjectMapper objectMapper = new ObjectMapper();
        when(walletService.save(wallet)).thenThrow(new WalletByNameAlreadyExists());

        mockMvc.perform(post("/users/1/wallets")
                .content(objectMapper.writeValueAsString(wallet))
                .contentType(MediaType.APPLICATION_JSON));

        verify(walletService, times(1)).save(any(Wallet.class));
    }

    @Test
    void shouldGetWalletsForUserWithId1() throws Exception {
        User user = userRepository.save(new User("admin", "password", "John", "Doe"));
        Wallet wallet = new Wallet("new-admin-wallet", 400, user);
        when(walletService.getByUserId(eq(1))).thenReturn(Arrays.asList(wallet));

        final ResultActions getWalletsByUserId = mockMvc.perform(get("/users/1/wallets")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        verify(walletService, times(1)).getByUserId(eq(1));

        getWalletsByUserId
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].name").value("new-admin-wallet"))
                .andExpect(jsonPath("$[0].balance").value("400.0"));
    }
}
