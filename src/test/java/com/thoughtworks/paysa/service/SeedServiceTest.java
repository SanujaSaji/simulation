package com.thoughtworks.paysa.service;

import com.thoughtworks.paysa.model.User;
import com.thoughtworks.paysa.model.Wallet;
import com.thoughtworks.paysa.repository.UserRepository;
import com.thoughtworks.paysa.repository.WalletRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@AutoConfigureMockMvc
class SeedServiceTest {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private WalletRepository walletRepository;

    @Autowired
    private SeedService seedService;

    @Test
    void userShouldBeSeeded() {
        User user = seedService.seedUser();

        assertEquals("John", userRepository.findById(user.getId()).orElse(new User()).getFirstName());
    }

    @Test
    void walletShouldBeSeeded() {
        Wallet wallet = seedService.seedWallet();

        assertEquals("admin-wallet", walletRepository.findById(wallet.getId()).orElse(new Wallet()).getName());
    }
}
