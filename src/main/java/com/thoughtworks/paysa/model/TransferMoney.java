package com.thoughtworks.paysa.model;

public class TransferMoney {
    Wallet fromWallet;
    Wallet toWallet;
    Transaction transaction;

    public TransferMoney() {
    }

    public TransferMoney(Wallet fromWallet, Wallet toWallet, Transaction transaction) {
        this.fromWallet = fromWallet;
        this.toWallet = toWallet;
        this.transaction = transaction;
    }

    public Wallet getFromWallet() {
        return fromWallet;
    }

    public void setFromWallet(Wallet fromWallet) {
        this.fromWallet = fromWallet;
    }

    public Wallet getToWallet() {
        return toWallet;
    }

    public void setToWallet(Wallet toWallet) {
        this.toWallet = toWallet;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }
}
