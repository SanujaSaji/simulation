package com.thoughtworks.paysa.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "wallet", uniqueConstraints = {@UniqueConstraint(columnNames = {"name", "user_id"})})
public class Wallet {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    private double balance;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private User user;
    private Date creationDate;

    public Wallet() {
    }

    public Wallet(String name, int balance, User user) {
        this.creationDate=new Date();
        this.name = name;
        this.balance = balance;
        this.user = user;
    }

    public String getName() {
        return name;
    }

    public double getBalance() {
        return balance;
    }

    public int getId() {
        return id;
    }

    public User getUser() {
        return user;
    }

    public void credit(double amount) {
        balance += amount;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public void debit(double amount) {
        balance -= amount;
    }
}


