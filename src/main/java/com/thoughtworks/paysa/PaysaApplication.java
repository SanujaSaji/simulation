package com.thoughtworks.paysa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PaysaApplication {

	public static void main(String[] args) {
		SpringApplication.run(PaysaApplication.class, args);
	}
}
