package com.thoughtworks.paysa.service;

import com.thoughtworks.paysa.exception.InvalidTransaction;
import com.thoughtworks.paysa.exception.WalletNotFoundException;
import com.thoughtworks.paysa.model.Transaction;
import com.thoughtworks.paysa.model.TransactionType;
import com.thoughtworks.paysa.model.TransferMoney;
import com.thoughtworks.paysa.model.Wallet;
import com.thoughtworks.paysa.repository.TransactionRepository;
import com.thoughtworks.paysa.repository.WalletRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class TransactionService {

    @Autowired
    TransactionRepository transactionRepository;

    @Autowired
    private WalletRepository walletRepository;

    public Transaction save(int walletId, Transaction transaction) throws WalletNotFoundException, InvalidTransaction {
        if (transaction.getAmount() == 0) {
            throw new InvalidTransaction("Amount should be greater than zero");
        }
        Wallet wallet = walletRepository.findById(walletId).orElseThrow(WalletNotFoundException::new);
        transaction.setWallet(wallet);
        transaction.setCreationDate(new Date());
        return transactionRepository.save(transaction);
    }

    public List<Transaction> getByWalletId(int walletId) {
        List<Transaction> transactions = transactionRepository.findByWalletId(walletId);
        transactions.sort((o1, o2) -> o2.getCreationDate().compareTo(o1.getCreationDate()));
        return transactions;
    }

    public Transaction transfer(TransferMoney transferMoney) throws InvalidTransaction, WalletNotFoundException {
        if (transferMoney.getTransaction().getAmount() < 50) {
            throw new InvalidTransaction("Minimum amount should be greater than 50");
        }
        Wallet fromWallet;
        final Optional<Wallet> fromWalletCheck = walletRepository.findById(transferMoney.getFromWallet().getId());
        if(!fromWalletCheck.isPresent()){
            throw new WalletNotFoundException();
        }
        fromWallet = fromWalletCheck.get();

        Wallet toWallet;
        final Optional<Wallet> toWalletCheck = walletRepository.findById(transferMoney.getToWallet().getId());
        if(!toWalletCheck.isPresent()){
            throw new WalletNotFoundException();
        }
        toWallet = toWalletCheck.get();

        Transaction transaction = transferMoney.getTransaction();
        if (transaction.getAmount() > fromWallet.getBalance()) {
            throw new InvalidTransaction("Insufficient balance in wallet");
        }

        Transaction debitTransaction = new Transaction(new Date(), transaction.getRemarks(),
                transaction.getType(), transaction.getAmount(), fromWallet, toWallet.getName());
        fromWallet.debit(transaction.getAmount());
        walletRepository.save(fromWallet);

        Transaction creditTransaction = new Transaction(new Date(), transaction.getRemarks(),
                TransactionType.CREDIT, transaction.getAmount(), toWallet, fromWallet.getName());
        toWallet.credit(transaction.getAmount());
        walletRepository.save(toWallet);

        debitTransaction = transactionRepository.save(debitTransaction);
        creditTransaction = transactionRepository.save(creditTransaction);

        return debitTransaction;
    }
}
