package com.thoughtworks.paysa.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
public class InvalidTransaction extends Exception {
    public InvalidTransaction(String errorMessage){
        super(errorMessage);
    }
}
