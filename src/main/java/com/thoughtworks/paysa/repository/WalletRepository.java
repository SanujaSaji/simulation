package com.thoughtworks.paysa.repository;

import com.thoughtworks.paysa.model.Wallet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WalletRepository extends JpaRepository<Wallet, Integer> {
    List<Wallet> findByName(String name);
    List<Wallet> findByUserId(int userId);

}